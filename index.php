<!-- Fallback Site with no content - necessary -->
<?php get_header(); ?>

<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>    

  <div class="card my-1">
      <h3 class="heading-post">
        <!-- Display the title -->
        <?php the_title(); ?>
      </h3>

      <p>
        <!-- Show max. 55 characters/ words -->
          <?php the_excerpt(); ?>
      </p>

      <small>
        <!-- Display the post date -->
        <span class="date">
          <?php echo get_the_date();?>
        </span>

        <!-- Display the categories -->
        <?php echo get_the_category_list(', '); ?>
      </small>

    </div>

  <?php endwhile;
    /* Pagination, with two shown pages between next and prev */
    the_posts_pagination( array( 'mid_size' => 2 ) ); 

    else: ?>

    <p>Deine Suche ergab leider keine treffer</p>

<?php endif; ?>

<?php get_footer(); ?>