<?php get_header(); ?>

  <div class="container container-impressum-datenschutz min-vh">
    <!-- Title from the site '(backend)' -->
    <h1 class="my-2 h-impressum-datenschutz"><?php the_title(); ?></h1>

    <?php 
    if ( have_posts() ) : ?>

    <div class="p-impressum-datenschutz">
      <?php while ( have_posts() ) :
        the_post(); ?>
        <!-- Content from backend -->
        <?php the_content(); ?>

      <?php  endwhile; else:endif;?>
    </div>

  </div>

<?php get_footer(); ?>