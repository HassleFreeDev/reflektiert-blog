<div class="search">
  <form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
      <label class="screen-reader-text" for="s"></label>
        <input type="text" value="" name="s" id="s" placeholder="Beiträge durchsuchen" required/>
        <input class="search-btn " type="submit" id="searchsubmit" value="Suchen"/>
  </form>
</div>