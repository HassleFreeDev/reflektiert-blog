<!-- Include header.php -->
<?php get_header(); ?>

<!-- Default page with latest blog posts next to home.php '(front-page with latest blog posts)'-->
<div class="container min-vh">
     <!-- Include/import template part -->
     <?php get_template_part('includes/section', 'post'); ?>
</div>

<!-- Include footer.php -->
<?php get_footer(); ?>