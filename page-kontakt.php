<?php get_header(); ?>

  <div class="container min-vh">
    <main id="main-kontakt">
      <div class="content-wrap my-4">
      <!-- div for grid layout -->
        <div>
          <header id="head-kontakt" class="my-1">
            <h1>
                <?php the_title(); ?>
            </h1>
          </header>

          <div class="content-wrap-paragraph">
            <!-- Get content from the backend - written in 'Über' site -->
            <?php if ( have_posts() ) : 
                while ( have_posts() ) : 
                  the_post(); ?>
          
                    <?php the_content(); ?>

            <?php endwhile; else : ?>
            <!-- else : If there is no content -->
              <p><?php esc_html_e( 'Ups... da ist etwas schief gelaufen!' ); ?></p>
            <?php endif; ?>
          </div>
          
        </div>
          <!-- Get image set in backend via ACF from 'Über' site -->
          <?php 
            $image = get_field('kontakt_image');
            if( !empty( $image ) ): ?>
              <!-- Image -->
              <img src="<?php echo esc_url($image['url']); ?>" class="image-kontakt" alt="<?php echo esc_attr($image['alt']); ?>" />

          <?php endif; ?>
        </div>
    </main>  
  </div>

  <div class="test">
              <div class="test2"></div>
  </div>

<?php get_footer(); ?>