<?php

/* Enqueue stylesheets */
  function load_stylesheets() {
    wp_register_style('main', get_template_directory_uri() . '/dist/css/main.css', array(), '', 'all');
    wp_enqueue_style('main');

    wp_register_style('font-awesome', get_template_directory_uri() . '/node_modules/@fortawesome/fontawesome-free/css/all.css', array(), '', 'all');
    wp_enqueue_style('font-awesome');

  }
  add_action('wp_enqueue_scripts', 'load_stylesheets');

/* Enqueue scripts*/
  function load_scripts() {
    wp_register_script('app', get_template_directory_uri() . '/dist/js/app.js', array(), '', true);
    wp_enqueue_script('app');
    
  }
  add_action('wp_enqueue_scripts', 'load_scripts');

/* Add custom header to customizer */
function reflektiert_cstm_header() {
  $args = array(
      'default-image'      => get_template_directory_uri() . '/img/reflektiert_logo.png',
      'default-text-color' => 'fff',
      'width'              => 1000,
      'height'             => 250,
      'flex-width'         => true,
      'flex-height'        => true,
  );
  /* Add theme support (make it avaiable in the customizer) */
  add_theme_support( 'custom-header', $args);
}
add_action( 'after_setup_theme', 'reflektiert_cstm_header' );


/* Add ACF options site for customisation purpose*/
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

/* Add cutsom menu to backend */
function register_custom_menus() {
  register_nav_menus(
    array(
      'header-menu'     => __( 'Header Menu' ),
      'footer-menu'     => __( 'Footer Menu' ),
      'categories-menu' => __( 'Kategorien' )
     )
   );
 }
 add_action( 'init', 'register_custom_menus' );

/* Exclude pages from the search.php if is not admin (not in the backend) */
 if(!is_admin()) {
  function wpb_search_filter($query) {
    if ($query->is_search) {
    $query->set('post_type', 'post');

    return $query;
    }
  }

add_filter('pre_get_posts','wpb_search_filter');
}

/* Filter the excerpt length to 'n' words */
function wpdocs_custom_excerpt_length( $length ) {
  return  100;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );


/* Link the <Read More> (usage with <?php the_excerpt(); ?>) to the Post */
function wpdocs_excerpt_more($more) {
  if ( !is_single() ) {
      $more = sprintf( '<div class="read-more-wrap"> <a class="read-more" href="%1$s">%2$s</a> </div>',
          get_permalink( get_the_ID() ),
          __( 'Lies mehr', 'textdomain' )
      );
  }

  return $more;
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

/* Adding dots behind the excerpt text */
function new_excerpt_more( $more ) {
  return ' [...]';
}
add_filter('excerpt_more', 'new_excerpt_more');
