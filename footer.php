<!-- Set background color -->
<div class="footer-wrap">
<!-- Set max-width and flex -->
  <div class="container wrap-content">

    <div class="wrap-follow">
      <p>Folge mir auf Instagram</p>
      <a class="insta-icon" href="https://www.instagram.com/psreflektiert/" target="_blank" rel="noopener">
        <i class="fab fa-instagram"></i>
      </a>
    </div>

    <div class="wrap-logo-footer">
      <!-- Positioning footer-menu from backend  -->
      <?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>

      <!-- Insert Footer Logo / Image -->
      <?php 
        $get_image = get_field('footer_logo', 'option');

        if( !empty( $get_image ) ): ?>
            <img class="footer-image" src="<?php echo esc_url($get_image['url']); ?>" alt="<?php echo esc_attr($get_image['alt']); ?>"/>

      <?php endif; ?>
    </div>

  </div>
</div>

<!-- Insert needed scripts and so on - !important -->
<?php wp_footer(); ?>
</body>
</html>