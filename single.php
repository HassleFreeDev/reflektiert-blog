<!-- Include header.php -->
<?php get_header(); ?>

<div class="container-grid min-vh">
   <div class="post-grid">

   <?php if ( have_posts() ) : ?>
	  <?php while ( have_posts() ) : the_post(); ?>    

  <div class="card my-1">
      <h3 class="heading-post">
        <!-- Display the title -->
        <?php the_title(); ?>
      </h3>

      <p>
        <!-- Show max. 55 characters/ words -->
        <?php the_content(); ?>
      </p>
      
      <small>
        <!-- Display the post date -->
        <span class="date">
          <?php echo get_the_date();?>
        </span>

        <!-- Display the categories -->
        <?php echo get_the_category_list(', '); ?>
      </small>
    </div>

  <?php endwhile; else: endif; ?>
  
   </div>
   <div class="categories-grid">
      <div class="categories my-1">
         <h3>Kategorien</h3>
         
         <!-- Custom categories menu -->
         <?php wp_nav_menu( array( 'theme_location' => 'categories-menu' ) ); ?>
      </div>
   </div>

</div>

<!-- Include footer.php -->
<?php get_footer(); ?>