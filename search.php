<?php get_header(); ?>

<div class="container-grid">
  <div class="search-result-number my-1">
    <!-- Put search result output //number// in a variable -->
    <?php $count = $wp_query->found_posts; ?>
  
    <!-- Output variable -->
    <div class="search-count">
      <?php echo $count; ?> 
    </div>
  
    <div class="search-output">
      <!-- Condition if output is 1 -->
      <?php if($count == 1) {
        _e( 'Suchergebnis für: ', 'locale' ); ?>
        <div class="search-input">
          <?php the_search_query();?>
        </div>
      <!-- or higher / lower than 1  -->
      <?php } else {
        _e( 'Suchergebnisse für: ', 'locale' ); ?>
        <div class="search-input">
          <?php the_search_query(); ?>
        </div>
      <?php } ?>
    </div>
  </div>
</div>

<div class="container-grid min-vh">
  <div class="post-grid">

    <?php if ( have_posts() ) : ?>
	    <?php while ( have_posts() ) : the_post(); ?>    

    <div class="card my-1">
        <h3 class="heading-post">
          <!-- Display the title -->
          <?php the_title(); ?>
        </h3>

        <p>
          <!-- Show max. 55 characters/ words --> 
            <?php the_excerpt(); ?>
        </p>

        <div class="read-more-wrap">
          <a href="<?php the_permalink(); ?>" class="read-more">Lies mehr</a>
        </div>

        <small>
          <!-- Display the post date -->
          <span class="date">
            <?php echo get_the_date();?>
          </span>
          <!-- Display the categories -->
          <?php echo get_the_category_list(', '); ?>
        </small>
      </div>

        <?php endwhile; 
        /* Pagination, with two shown pages between next and prev */
        the_posts_pagination( array( 'mid_size' => 2 ) );
          
        else: endif; ?>
    </div>

    <div class="categories-grid">
      <div class="categories my-1">
        <h3>Kategorien</h3>
        
        <!-- Custom categories menu -->
        <?php wp_nav_menu( array( 'theme_location' => 'categories-menu' ) ); ?>
        
      </div>
    </div>
</div>

<?php get_footer(); ?>