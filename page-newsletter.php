<?php get_header(); ?>

<?php 
    $acfHeadingNewsletter = get_field('acf_newsletter_heading');
?>

<div class="container-grid min-vh my-2">
    <div class="post-grid grid-newsletter">
        <!-- <h2><?php echo $acfHeadingNewsletter; ?></h2>

        <div class="container-newsletter-registrierung my-2">
            <div>
                <input class="newsletter" type="email" name="EMAIL" placeholder="Deine E-Mail Adresse" required />
                
                <input class="search-btn" type="submit" value="Registrieren" />
            </div>
        </div> -->
    

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

        <?php the_content(); ?>

    <?php endwhile; else : ?>
	    <p><?php esc_html_e( 'Da ist wohl irgendetwas schief gelaufen :O' ); ?></p>
    <?php endif; ?>

    </div>
    
</div>

<?php get_footer(); ?>