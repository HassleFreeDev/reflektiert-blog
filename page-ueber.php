<?php get_header(); ?>

  <div class="container min-vh">
      <main id="main-ueber">
        <div class="content-wrap my-4">
          <!-- Get image set in backend via ACF from 'Über' site -->
          <?php 
            $image = get_field('ueber_image');
            if( !empty( $image ) ): ?>
              <!-- Image -->
              <img src="<?php echo esc_url($image['url']); ?>" class="image-ueber" alt="<?php echo esc_attr($image['alt']); ?>" />

          <?php endif; ?>

          <div>
            <header id="head-ueber" class="my-1">
              <h1>
                  <?php the_title(); ?>
              </h1>
            </header>

            <div class="content-wrap-paragraph">
              <!-- Get content from the backend - written in 'Über' site -->
              <?php if ( have_posts() ) : 
                  while ( have_posts() ) : 
                    the_post(); ?>
            
                      <?php the_content(); ?>

                <?php endwhile; else : ?>
                  <p><?php esc_html_e( 'Ups... da ist etwas schief gelaufen!' ); ?></p>
                <?php endif; ?>
            </div>
          </div>

        </div>
      </main>
  </div>

<?php get_footer(); ?>