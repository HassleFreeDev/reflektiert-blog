<!-- Include header.php -->
<?php get_header(); ?>

<!-- Default page with latest blog posts -->
<div class="container-grid min-vh">
   <div class="post-grid">
      <?php get_template_part('includes/section', 'post'); ?>
   </div>
   
   <!-- Display categories -->
   <div class="categories-grid">
      <div class="categories my-1">
         <h3>Kategorien</h3>
         <!-- Custom categories menu -->
         <?php wp_nav_menu( array( 'theme_location' => 'categories-menu' ) ); ?>
      </div>
   </div>
</div>

<!-- Include footer.php -->
<?php get_footer(); ?>