<?php get_header(); ?>

<div class="container-grid">
  <div class="category-main">
    <h3>Kategorie: </h3>
  
    <span>
      <!-- Only get / output the first category -->
      <?php
        $category = get_the_category(); 
        echo $category[0]->cat_name;
      ?>
    </span>
    
    
  </div>
</div>

<div class="container-grid min-vh">
   <div class="post-grid">
  <!-- Loop -->
   <?php if ( have_posts() ) : ?>
	  <?php while ( have_posts() ) : the_post(); ?>    

  <div class="card my-1">
      <h3 class="heading-post">
        <!-- Display the title -->
        <?php the_title(); ?>
      </h3>
      <p>
        <!-- Show max. 55 characters/ words '(change in functions.php)'-->
          <?php the_excerpt(); ?>
      </p>

      <div class="read-more-wrap">
        <a href="<?php the_permalink(); ?>" class="read-more">Lies mehr</a>
      </div>
      
      <small>
        <!-- Display the post date -->
        <span class="date">
          <?php echo get_the_date();?>
        </span>

        <!-- Display the categories -->
        <?php echo get_the_category_list(', '); ?>
      </small>
    </div>

  <?php endwhile; 
  /* Pagination, with two shown pages between next and prev */
  the_posts_pagination( array( 'mid_size' => 2 ) ); 
  
  else: endif; ?>
  
  </div>
  <div class="categories-grid">
    <div class="categories my-1">
        <h3>Kategorien</h3>
        
        <!-- Custom categories menu -->
        <?php wp_nav_menu( array( 'theme_location' => 'categories-menu' ) ); ?>
        
      </div>
  </div>

</div>


<?php get_footer(); ?>