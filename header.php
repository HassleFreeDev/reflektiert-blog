<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">
  <!-- Display site name, set in the backend, in the tab bar -->
  <title><?php bloginfo('name'); ?></title>
  <!-- Insert all needed head tags - !important -->
  <?php wp_head(); ?>
</head>
<body>
<!-- Mobile navigation -->
<nav id="main-nav-mobile">
  <div class="navigation-heading">
    <h4>Navigation</h4>
    <!-- Close button inside opened navbar -->
    <div id="close-btn">
      <div class="x-left"></div>
      <div class="x-right"></div>
    </div>
  </div>
  <!-- Searchbar inside mobile navbar -->
  <div class="search-mobile">
    <!-- Insert searchbar 'searchform.php'-->
    <?php get_search_form(); ?>
  </div>

  <div class="navigation-mobile">
    <h3>Seiten</h3>
    <!-- Custom head menu -->
    <?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
  </div>

  <div class="categories-mobile">
    <h3>Kategorien</h3>
    <!-- Custom categories menu -->
    <?php wp_nav_menu( array( 'theme_location' => 'categories-menu' ) ); ?>
  </div>
</nav>

<!-- Navigation / Head desktop view -->
<div class="head-wrap">
  <div class="logo">
    <!-- Link to front '(Blog)' page '(home.php in this case) -->
    <a href="<?php echo get_option("siteurl"); ?>">
      <img class="header-image" alt="Reflektiert Logo" src="<?php header_image(); ?>" width="<?php echo absint( get_custom_header()->width ); ?>" height="<?php echo absint( get_custom_header()->height ); ?>">
    </a>
  </div>
  
  <div class="navigation-wrap">
    <!-- Insert searchbar 'searchform.php'-->
    <?php get_search_form(); ?>


    <nav id="main-nav">
      <!-- Custom head menu -->
        <?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
    </nav>
  </div>

  <div id="navigation-wrap-mobile">
      <div class="burger"></div>
  </div>
</div>

  
  
