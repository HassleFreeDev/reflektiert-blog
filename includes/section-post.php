<!-- Get the content from the posts // from the backend -->
<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>    

  <div class="card my-1">
      <h3 class="heading-post">
        <!-- Display the title -->
        <?php the_title(); ?>
      </h3>

      <p>
        <!-- Show max. 55 characters/ words -->
          <?php the_excerpt(); ?>
      </p>

      <div class="read-more-wrap">
        <a href="<?php the_permalink(); ?>" class="read-more">Lies mehr</a>
      </div>

      <small>
        <!-- Display the post date -->
        <span class="date">
          <?php echo get_the_date();?>
        </span>

        <!-- Display the categories -->
        <?php echo get_the_category_list(', '); ?>
      </small>

    </div>

  <?php endwhile;
    /* Pagination, with two shown pages between next and prev */
    the_posts_pagination( array( 'mid_size' => 2 ) ); 

    else: ?>

      <div class="container error">
        <div class="error-msg">
            
          <p>Zurzeit sind keine Beiträge verfügbar</p>
          
        </div>
      </div>
    
<?php endif; ?>
    
<!-- Display the following if the displayed site is search.php -->