<?php get_header(); ?>
<!-- 404 page -->
<div class="container error">
  <div class="error-msg">
      
     <p>
        <?php _e('404: Die Seite die du suchst existiert nicht', 'textdomain'); ?>
    </p>
    
  </div>
</div>

<?php get_footer(); ?>