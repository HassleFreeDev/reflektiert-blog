const navigationWrapMobile = document.getElementById('navigation-wrap-mobile');
const mainNavMobile = document.getElementById('main-nav-mobile');
const closeBtn = document.getElementById('close-btn');
const body = document.getElementsByTagName('body');


navigationWrapMobile.addEventListener('click', () => {
  mainNavMobile.classList.add('navbar-open');
});

closeBtn.addEventListener('click', () => {
  mainNavMobile.classList.remove('navbar-open');
});